package com.manage.users;

import com.manage.users.controller.UserController;
import com.manage.users.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UsersApplication implements CommandLineRunner {

	@Autowired
	private UserController userController = new UserController();

	public static void main(String[] args) {
		SpringApplication.run(UsersApplication.class, args);
	}

	@Override
	public void run(String[] args) throws Exception {
//
//		userController.createUser(new User("","test1","test1@.co.uk"));
//		userController.createUser(new User("","test2","test2@.co.uk"));
//		userController.createUser(new User("","test3","test3@.co.uk"));

	}

}
