package com.manage.users.controller;

import com.manage.users.model.User;
import com.manage.users.service.UserServ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/users")
public class UserController {


    private String index = "users";

    private UserServ service;

    @Autowired
    public UserController(UserServ service) {
        this.service = service;
    }
    public UserController() { }

    @DeleteMapping("/del/{id}")
    public List<User> delete(@PathVariable("id") String id) throws IOException {
        return service.deleteUser(index, id);
    }

    @RequestMapping(path = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public List<User> createUser(@RequestBody User user) throws IOException {
        return service.indexUser(user, index, user.getId());
    }

    @GetMapping("/all")
    public List<User> getUsers() throws IOException {
        return service.getAll();
    }

}