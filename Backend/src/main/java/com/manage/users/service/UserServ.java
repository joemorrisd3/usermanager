package com.manage.users.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.manage.users.controller.UserController;
import com.manage.users.model.User;
import org.apache.http.HttpHost;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthRequest;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.ReindexRequest;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class UserServ {

    /**
     * High level rest client for querying the database.
     */
    private RestHighLevelClient client;

    /**
     * Used to map Objects to a JSON value.
     */
    private ObjectMapper objectMapper;

    private String ELASTICHOST;
    private int ELASTICPORT;

    /**
     * Default initialisation for the Rest Client.
     */
    @Autowired
    public UserServ() {
        this.ELASTICHOST = "elastic";
        this.ELASTICPORT = 9200;
        this.client = new RestHighLevelClient(RestClient.builder(new HttpHost(ELASTICHOST, ELASTICPORT, "http")));
        this.objectMapper = new ObjectMapper();
    }


    /**
     * Service for returning all users within an index.
     * @return all users.
     * @throws IOException
     */
    public List<User> getAll() throws IOException {
        SearchRequest searchRequest = new SearchRequest();
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        searchSourceBuilder.size(10000);
        searchRequest.requestCache(false);
        searchRequest.source(searchSourceBuilder);

        SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
        SearchHits hits = searchResponse.getHits();

        List<User> users = new ArrayList<>();
        for(SearchHit hit : hits)
        {
            User u = new User();
            u.setId(hit.getId());
            User jsonUser = objectMapper.readValue(hit.getSourceAsString(), User.class);
            u.setName(jsonUser.getName());
            u.setEmail(jsonUser.getEmail());
            users.add(u);
        }
        return users;
    }

    /**
     * Service for creating and updating request through indexing.
     * @param user user object passed in to be able to add to db.
     * @param index index of database hardcoded as users.
     * @param id ID of user, blank if new, data if not new.
     * @return all users.
     * @throws IOException
     */
    public List<User> indexUser(User user, String index, String id) throws IOException {
        String jsonUser = objectMapper.writeValueAsString(user);
        IndexRequest indexReq = new IndexRequest(index).id(id).source(jsonUser, XContentType.JSON);
        indexReq.setRefreshPolicy(WriteRequest.RefreshPolicy.IMMEDIATE);
        IndexResponse response = null;
        try {
            response = client.index(indexReq, RequestOptions.DEFAULT);
        } catch(IOException ex) {
        }

        return getAll();
    }

    /**
     * Service for deleting id's within an index.
     * @param index index to look for value.
     * @param id of document to delete.
     * @return all users.
     * @throws IOException
     */
    public List<User> deleteUser(String index, String id) throws IOException {
        DeleteRequest request = new DeleteRequest(
                index,
                id);
        request.setRefreshPolicy(WriteRequest.RefreshPolicy.IMMEDIATE);

        DeleteResponse deleteResponse = client.delete(request, RequestOptions.DEFAULT);

        return getAll();
    }

}
