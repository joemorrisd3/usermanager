FROM docker.elastic.co/elasticsearch/elasticsearch:7.3.2
RUN chown -R elasticsearch: /usr/share/elasticsearch/ /etc/elasticsearch/ /var/lib/elasticsearch /var/log/elasticsearch
