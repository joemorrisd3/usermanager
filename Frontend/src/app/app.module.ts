import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'
import { MatInputModule, MatPaginatorModule, MatProgressSpinnerModule, MatMenuModule,
         MatSortModule, MatTableModule, MatIconModule, MatDialogModule, MatButtonModule } from "@angular/material";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UsertableComponent } from './components/usertable/usertable.component';
import { UserServiceService } from './user-service.service';
import { EditComponent } from './components/dialog/edit/edit.component';
import { DeleteComponent } from './components/dialog/delete/delete.component';
import { AddComponent } from './components/dialog/add/add.component';

@NgModule({
  declarations: [
    AppComponent,
    UsertableComponent,
    EditComponent,
    DeleteComponent,
    AddComponent
  ],
  entryComponents: [
    AddComponent,
    EditComponent,
    DeleteComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatMenuModule
  ],
  providers: [UserServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
