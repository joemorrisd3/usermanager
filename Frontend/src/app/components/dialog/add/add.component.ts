import {Component, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { User } from '../../../user';
import { UserServiceService } from '../../../user-service.service';
import { MatTableDataSource } from '@angular/material';
import { FormGroup, FormBuilder, EmailValidator, Validators, FormControl } from '@angular/forms';
import { EditComponent } from '../edit/edit.component';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent {
  
  user: User[];
  editDataSource: MatTableDataSource<User>;
  myGroup: FormGroup;

  constructor(
  private userServiceService: UserServiceService,
  private formBuilder: FormBuilder,
  public dialogRef: MatDialogRef<EditComponent>,
  @Inject(MAT_DIALOG_DATA) public data: any) {
    this.userServiceService.findAll().subscribe(data => {
        this.user = data;
        this.editDataSource = new MatTableDataSource(this.user);
      });

    this.myGroup = formBuilder.group({
      name: new FormControl("", [
        Validators.required,
        Validators.maxLength(999) // just in case Hubtert Blaine Wolfe+988 inputs his name
      ]),
      email: new FormControl("", [
        Validators.required,
        Validators.email
      ])
    });

    dialogRef.disableClose = true;
  }
  
  onNoClick(): void {
    this.dialogRef.close(this.editDataSource);
  }

  onYesClick(): void {
    const addUser: User = <User> {
      id: "",
      name: this.myGroup.controls.name.value,
      email: this.myGroup.controls.email.value
    }
    this.userServiceService.addUser(addUser).subscribe( data => {
      this.dialogRef.close();
    });
  }
}
