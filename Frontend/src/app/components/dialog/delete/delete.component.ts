import {Component, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { User } from '../../../user';
import { UserServiceService } from '../../../user-service.service';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class DeleteComponent {

  user: User[];
  editDataSource: MatTableDataSource<User>;

  constructor(
  public dialogRef: MatDialogRef<DeleteComponent>,
  private userServiceService: UserServiceService,
  @Inject(MAT_DIALOG_DATA) public data: any) { 
    this.userServiceService.findAll().subscribe(data => {
      this.user = data;
      this.editDataSource = new MatTableDataSource(this.user);
    });
  }

  onNoClick(): void {
    this.dialogRef.close(this.editDataSource);
  }

  onYesClick(): void {
    this.userServiceService.deleteUser(this.data.user).subscribe( data => {
      this.dialogRef.close(data);
    });
  }

}
