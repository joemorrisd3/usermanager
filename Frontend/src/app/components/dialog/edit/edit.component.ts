import { Component, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material'
import { FormBuilder, FormGroup, FormControl, Validators, FormGroupDirective } from '@angular/forms';

import { User } from '../../../user';
import { UserServiceService } from '../../../user-service.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent {

  user: User[];
  editDataSource: MatTableDataSource<User>;
  myGroup: FormGroup;

  constructor(
  private userServiceService: UserServiceService,
  private formBuilder: FormBuilder,
  public dialogRef: MatDialogRef<EditComponent>,
  @Inject(MAT_DIALOG_DATA) public data: any) {
    data = data.userToUpd;
    this.myGroup = formBuilder.group({
      id: formBuilder.control(data.id),
      name: new FormControl(data.name, [
        Validators.required,
        Validators.maxLength(999)
      ]),
      email: new FormControl(data.email, [
        Validators.required,
        Validators.email
      ])
    });

    dialogRef.disableClose = true;
    
    this.userServiceService.findAll().subscribe(data => {
        this.user = data;
        this.editDataSource = new MatTableDataSource(this.user);
      });

    this.myGroup.controls.id.disable();
  }

  onNoClick(): void {
    this.dialogRef.close(this.editDataSource);
  }

  onYesClick(): void {
    const editedUser: User = <User> {
      id: this.myGroup.controls.id.value,
      name: this.myGroup.controls.name.value,
      email: this.myGroup.controls.email.value
    }
    this.userServiceService.addUser(editedUser).subscribe( data => {
      this.dialogRef.close(); 
   });
  }
}
