import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material'
import { MatPaginatorModule } from '@angular/material';
import { MatSort } from '@angular/material';
import { MatDialog, MatDialogRef, MatDialogConfig, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { User } from '../../user';
import { AddComponent } from '../dialog/add/add.component';
import { EditComponent } from '../dialog/edit/edit.component';
import { DeleteComponent } from '../dialog/delete/delete.component';
import { UserServiceService } from '../../user-service.service';

@Component({
  selector: 'app-usertable',
  templateUrl: './usertable.component.html',
  styleUrls: ['./usertable.component.css']
})
export class UsertableComponent implements OnInit {

  users: User[];
  arrCase : object [];
  dataSource: MatTableDataSource<User>;
  displayedColumns = ['id', 'name', 'email', 'actions'];

  constructor(private userServiceService: UserServiceService, public dialog: MatDialog) { 
    this.dataSource = new MatTableDataSource();
  }
  
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.refresh();
  }

  refresh() {
    this.userServiceService.findAll().subscribe(data => {
        this.users = data;
        this.dataSource.data = this.users;
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  addUser() {
    const temp: User = new User;
    let dialogRef = this.dialog.open(AddComponent, {
      data: { temp },
    }).afterClosed().subscribe(response => {
      this.refresh();
    });
  }
    
  deleteUser(userToDel: User) {
     let dialogRef = this.dialog.open(DeleteComponent, {
      data: { user: userToDel },
     }).afterClosed().subscribe(response => {
      this.refresh();
    });
   }

  updateUser(userToUpd: User) {
    let dialogRef = this.dialog.open(EditComponent, {
      data: { userToUpd }
    }).afterClosed().subscribe(response => {
      this.refresh();
    });
  
  }
}
