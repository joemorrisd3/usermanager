import { Injectable, SystemJsNgModuleLoader } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from './user';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  private usersUrl: string;

  constructor(private http: HttpClient) {
    this.usersUrl = 'http://' +  window.location.hostname + ':8080/users';
  }

  public findAll(): Observable<User[]> {
    return this.http.get<User[]>(this.usersUrl + "/all");
  }

  public deleteUser(id : String) {
    return this.http.delete<User>(this.usersUrl + "/del/" + id);
  }

  public addUser(user) {
    return this.http.post<User>(this.usersUrl + "/add", user);
  }
}
